SELECT
  post_id,
  post_title,
  post_name,
  sponsor_date_start AS last_sponsor_date_start,
  CASE
    WHEN NOT active_sponsor THEN DATEDIFF(curdate(), sponsor_date_end)/30
    ELSE NULL
  END AS ending_sponsoring_elapsed_months,

  sponsor_type AS last_sponsor_type,
  website

FROM  fVT6y_posts AS posts
JOIN (
  SELECT
  post_id, sponsor_date_start, sponsor_type, website,
  DATE_ADD(sponsor_date_start, INTERVAL 1 YEAR) AS sponsor_date_end,
  DATEDIFF(curdate(), sponsor_date_start) <= 365 AS active_sponsor
  FROM (
    SELECT post_id,
    GROUP_CONCAT( if(meta_key='sponsor_month',meta_value,NULL) ) AS sponsor_month,
    GROUP_CONCAT( if(meta_key='sponsor_year',STR_TO_DATE(meta_value, '%Y%m%d'),NULL) ) AS sponsor_date_start,
    GROUP_CONCAT( if(meta_key='sponsor_type',meta_value,NULL) ) AS sponsor_type,
    GROUP_CONCAT( if(meta_key='website',meta_value,NULL) ) AS website
    FROM fVT6y_postmeta
    GROUP BY post_id
  ) a
) meta
  ON (posts.ID = meta.post_id)
WHERE post_type = 'sponsor' and not active_sponsor
ORDER BY active_sponsor,ending_sponsoring_elapsed_months;

#!/bin/bash
set -e

function help {
  echo "usage:"
  echo "generate_mail_from_sql <sudbirectory> <name of report>"
  echo "Example:"
  echo "generate_mail_from_sql board-priv sponsors_monthly_report"
}

if [ "$#" -ne 2 ]; then
  help; exit
fi

if [ "$1" = "-h" ]; then
  help; exit
fi


DIR="$1"
REPORT="$2"

I_PATH="/home/cvvergara/wp-dbqueries/${DIR}"
O_PATH="/home/cvvergara/wp-dbqueries/csv"
I_FILE="${I_PATH}/${REPORT}.sql"
O_FILE="$(date +%F)-${REPORT}.csv"

source board-priv/config.sh


SUBJECT="${O_FILE}"
BOUNDRY="19032019ABCDE$(date +%F)"

{
echo "From: wordpress@osgeo.org"
echo "To: cro@osgeo.org"
echo "Disposition-Notification-To: vicky@georepublic.de"
echo "Subject: $SUBJECT"
echo "Reply-To: no-reply@osgeo.org"
echo "Mime-Version: 1.0"
echo "Content-Type: multipart/mixed; boundary=\"${BOUNDRY}\""
echo
echo "--${BOUNDRY}"
echo "Content-Type: text/plain; charset=\"US-ASCII\""
echo "Content-Transfer-Encoding: 7bit"
echo "Content-Disposition: inline"
echo
echo "Atached file: ${O_FILE}"
echo "Format: tab separated"
echo
echo "--${BOUNDRY}"
echo "Content-Type: text/csv; name=\"${O_FILE}\"";
echo "Content-Transfer-Encoding: base64"
echo "Content-Disposition: attachment; filename=\"${O_FILE}\""
echo ""
mysql -u ${DB_USER} --password=${DB_PW} --database=osgeo < "${I_FILE}" | base64
echo ""
echo "--${BOUNDRY}--";
} | sendmail -F "OSGeo Wordpress" -t

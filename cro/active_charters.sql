/*
List of active charter members
*/
SELECT year_elected, charter_id, user_login AS osgeo_id, last_name, first_name, is_active, user_id, user_email
FROM (
SELECT user_id,
GROUP_CONCAT( if(meta_key='charter_year_added',meta_value,NULL) ) AS year_elected,
GROUP_CONCAT( if(meta_key='last_name',meta_value,NULL) ) AS last_name,
GROUP_CONCAT( if(meta_key='first_name',meta_value,NULL) ) AS first_name,
GROUP_CONCAT( if(meta_key='ch_id',meta_value,NULL)) AS charter_id,

GROUP_CONCAT( if(meta_key='is_a_charter_member',meta_value='Active',NULL) ) AS is_active

FROM fVT6y_usermeta
GROUP BY user_id) a
JOIN fVT6y_users ON (user_id=id)
WHERE is_active
ORDER BY is_active DESC, year_elected, charter_id;

/*
List for ballots
*/
SELECT charter_id, concat(last_name," ", first_name) as name,  user_email
FROM (
SELECT user_id,
GROUP_CONCAT( if(meta_key='ch_id',meta_value,NULL)) AS charter_id,
GROUP_CONCAT( if(meta_key='last_name',meta_value,NULL) ) AS last_name,
GROUP_CONCAT( if(meta_key='first_name',meta_value,NULL) ) AS first_name,

GROUP_CONCAT( if(meta_key='is_a_charter_member',meta_value='Active',NULL) ) AS is_active

FROM fVT6y_usermeta
GROUP BY user_id) a
JOIN fVT6y_users ON (user_id=id)
WHERE is_active
ORDER BY cast(charter_id as unsigned);
